package com.cornel.queens_mutlithread

/*
 * Argument #1 - Board size
 * Argument #2 - Thread count
 * */

fun main(args: Array<String>) {
    solveQueensBoardProblem(args[0].toInt(), args[1].toInt(), args[2].toBoolean())
}