package com.cornel.queens_mutlithread

class QueensBoardPartCalculator(private val boardSideSize: Int,
                                private val startCell: Int = 0,
                                private val cellCount: Int = boardSideSize) : Thread() {

    private val solutions = mutableListOf<QueensBoard>()
    private var solved = false

    init {
        if (cellCount > boardSideSize) throw RuntimeException("Out of board bounds: a cells to process count" +
                " ($cellCount) is bigger than the board size ($boardSideSize).")
        if (startCell + cellCount > boardSideSize ) throw RuntimeException(
                "Out of board bounds: the processing will come out of the board bounds. The calculating starts " +
                "from the $startCell cell, and wants to process $cellCount cells, " +
                 "while the board side size is $boardSideSize")
    }

    override fun run() {
        if (boardSideSize == 1) {
            val board = QueensBoard(1)
            board.set(0, 0, true)
            solutions.add(board)
        } else {
            for (i in startCell..(startCell + cellCount - 1)) {
                val board = QueensBoard(boardSideSize)
                board.set(i, 0, true)
                val listOfAvailableRows = listOf(0..(boardSideSize - 1))
                        .flatten()
                        .filter { it != i }
                solve(board, 1, listOfAvailableRows)
            }
        }
        solved = true
    }

    private fun solve(board: QueensBoard, column: Int, availableRows: List<Int>) {

        if (column > board.sideSize) {
            throw RuntimeException("Something terrible happened: the column($column) " +
                    "> the board side size(${board.sideSize})")
        }

        if (column == board.sideSize) {
            solutions.add(board)
            return
        }

        for(i in availableRows) {
            if (board.checkDiagLeft(i, column)) {
                val nextStepBoard = board.clone()
                nextStepBoard.set(i, column, true)
                solve(nextStepBoard, column + 1, availableRows.filter { it != i })
            }
        }
    }

    fun getSolutions(): List<QueensBoard> {
        if (solved) {
            return solutions.toList()
        } else {
            throw RuntimeException("Board is not solved yet")
        }
    }

}