package com.cornel.queens_mutlithread

fun solveQueensBoardProblem(boardSideSize: Int = 8, threadCount: Int = boardSideSize, printAsBoard: Boolean = false) {
    val start = System.currentTimeMillis()
    var solutions = 0
    val normalizedTC = if (threadCount > boardSideSize) boardSideSize else threadCount
    val threads = ArrayList<QueensBoardPartCalculator>()

    val rowsPerThread = Math.ceil(boardSideSize.toDouble() / normalizedTC.toDouble()).toInt()

    for (i in 0..(boardSideSize - 1) step rowsPerThread) {
        val cellsToProduce = if (i + rowsPerThread > boardSideSize) boardSideSize - i else rowsPerThread
        threads.add(QueensBoardPartCalculator(boardSideSize, i, cellsToProduce))
    }

    threads.forEach {
        it.start()
        it.join()
    }
    val end = System.currentTimeMillis()

    threads.forEach {
        solutions += it.getSolutions().size
        if (printAsBoard) {
            it.getSolutions().forEach { println(it.toASCIIBoard()) }
        } else {
            it.getSolutions().forEach { println(it) }
        }
    }

    println("Total: $solutions solutions")
    println("Estimated time: ${end - start} ms")
}