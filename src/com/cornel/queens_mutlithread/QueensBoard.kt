package com.cornel.queens_mutlithread

class QueensBoard(val sideSize: Int) {
    private val size = sideSize * sideSize
    private var board = Array(size) { false }

    fun set(row: Int, col: Int, v: Boolean) {
        try {
            if (row > sideSize || col > sideSize) throw ArrayIndexOutOfBoundsException()

            board[sideSize * row + col] = v
        } catch (e: ArrayIndexOutOfBoundsException) {
            throw RuntimeException("Exception when trying to access [$row,$col], while board side size is $sideSize")
        }
    }

    private fun get(row: Int, col: Int): Boolean {
        try {
            if (row > sideSize || col > sideSize) throw ArrayIndexOutOfBoundsException()

            return board[sideSize * row + col]
        } catch (e: ArrayIndexOutOfBoundsException) {
            throw RuntimeException("Exception when trying to access [$row,$col], while board side size is $sideSize")
        }
    }

    /**
     * Check if the queens are on the cells on the left diagonals from [row, col]
     * */
    fun checkDiagLeft(row: Int, col: Int): Boolean {
        var j: Int
        for (i in 0..(sideSize - 1)) {
            if (i == row) continue

            j = if (i < row) col - row + i else col + row - i

            if (j < 0) continue

            if (get(i, j)) return false
        }
        return true
    }

    override fun toString(): String {
        val sb = StringBuilder()
        board.forEachIndexed {index, elem ->
            if (elem) sb.append("[ ${index / sideSize + 1}:${index % sideSize + 1} ],\t")
        }
        return sb.deleteCharAt(sb.length - 1).toString()
    }

    /**
     * Draws a board
     * */
    fun toASCIIBoard(): String {
        val sb = StringBuilder()

        for (i in 0..(sideSize - 1)) {
            for (j in 0..(sideSize - 1)) {
                if ((i + j) % 2 == 0) {
                    //White
                    sb.append(if (get(i, j)) QUEEN else CELL_WHITE )
                } else {
                    //Black
                    sb.append(if (get(i, j)) QUEEN else CELL_BLACK)
                }
            }
            sb.append('\n')
        }

        return sb.toString()
    }

    fun clone(): QueensBoard {
        val clone = QueensBoard(sideSize)
        clone.board = board.clone()
        return clone
    }


    companion object {
        private val CELL_WHITE  = 0x2593.toChar()
        private val CELL_BLACK  = 32.toChar()
        private val QUEEN       = 0x265B.toChar()
    }

}